# LaTeX-Vorlage für einen Regelkalender

Diese Vorlage erlaubt das Erstellen und Ausdrucken von Regelkalendern in
verschiedenen Varianten. Die Vorlage ist unabhängig vom aktuellen
Kalenderjahr, kann also immer wieder verwendet werden.

Es werden drei Kalender auf eine DIN-A4-Seite gedruckt.

## Systemvoraussetzungen

- eine aktuelle LaTeX-Version
- Programm zum Anzeigen und Ausdrucken von PDF-Dateien
- ein Drucker, der mindestens 600 DPI Druckauflösung hat

## Generierung der PDF-Datei

Die PDF-Datei kann über den Befehl `pdflatex Regelkalender.tex` im
Projektverzeichnis generiert werden.

## Bekannte Probleme

Gegebenenfalls ist der verwendete Drucker nicht in der Lage den Ausdruck
sauber zu Papier zu bringen.

## Anpassungen (Varianten)

Im oberen Bereich der Datei `Regelkalender.tex` sind einige Makros für
die Tabellenspalten definiert, die verdeutlichen wie die Ausgabe (z.B.
durch verschiedenfarbige Spalten) angepaßt werden kann.

```tex
%\newcolumntype{A}{>{\columncolor{gray!10!white}}m{4mm}}
%\newcolumntype{B}{>{\columncolor{gray!20!white}}m{4mm}}
%\newcolumntype{C}{>{\columncolor{gray!40!white}}m{4mm}}
\newcolumntype{D}{>{\columncolor{gray!20!white}}m{4mm}}
\newcolumntype{A}{m{4mm}}
\newcolumntype{B}{m{4mm}}
\newcolumntype{C}{m{4mm}}
%\newcolumntype{D}{m{4mm}}
```

Die Spalten haben folgende Bedeutung:

| Spalten | Bedeutung                  |
| :-----: | :------------------------- |
| A       | Kästchen 1 für Regelstärke |
| B       | Kästchen 2 für Regelstärke |
| C       | Kästchen 3 für Regelstärke |
| D       | Tag des Monats             |

### Beispiele für Varianten

Hier noch einige Beispiele für Varianten.

#### Tagesspalte grau hinterlegt

```tex
\newcolumntype{A}{m{4mm}}
\newcolumntype{B}{m{4mm}}
\newcolumntype{C}{m{4mm}}
\newcolumntype{D}{>{\columncolor{gray!20!white}}m{4mm}}
```

#### keine Farben

```tex
\newcolumntype{A}{m{4mm}}
\newcolumntype{B}{m{4mm}}
\newcolumntype{C}{m{4mm}}
\newcolumntype{D}{m{4mm}}
```

#### Stärkespalten mit grauem Farbverlauf

```tex
\newcolumntype{A}{>{\columncolor{gray!10!white}}m{4mm}}
\newcolumntype{B}{>{\columncolor{gray!20!white}}m{4mm}}
\newcolumntype{C}{>{\columncolor{gray!40!white}}m{4mm}}
\newcolumntype{D}{m{4mm}}
```

